package com.example.proyecto_01;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

public class EventReminder extends Application {

//    https://stackoverflow.com/questions/2002288/static-way-to-get-context-in-android

    private static Application sApplication;

    public static Application getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
    }

    public static NotificationCompat.Builder getNotificationNotificationBuilder(Activity activity) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(activity, "IdCanal");
        builder.setSmallIcon(R.drawable.ic_notify_on)
                .setContentTitle("Notification saved")
                .setContentText("Ejemplo de notificación en DAS.")
                .setVibrate(new long[]{250})
                .setAutoCancel(true);
        return builder;
    }
}
