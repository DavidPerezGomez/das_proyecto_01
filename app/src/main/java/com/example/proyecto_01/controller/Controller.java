package com.example.proyecto_01.controller;

import com.example.proyecto_01.EventReminder;
import com.example.proyecto_01.exceptions.NoSessionException;
import com.example.proyecto_01.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Controller {

    public static final String SORT_BY_NAME = Session.SORT_BY_NAME;
    public static final String SORT_BY_DATE = Session.SORT_BY_DATE;
    public static final String SORT_BY_DATE_ADDED = Session.SORT_BY_DATE_ADDED;

    private Controller() {

    }

    /**
     * Deletes the application database.
     *
     * @return True if the database was deleted successfully, False otherwise
     */
    public static boolean nukeDB() {
        return EventReminder.getContext().deleteDatabase("event_reminder");
    }

    /**
     * @return True if a session exists, False otherwise
     */
    public static boolean sessionIsOpen() {
        try {

            return Session.getSession() != null;
        } catch (NoSessionException e) {

            return false;
        }
    }

    /**
     * Attempt login with the given credentials.
     * If successful, create a new session for the user.
     *
     * @param email    User's email
     * @param password User's password
     * @return True if the credentials are correct and the user is logged in, False otherwise
     */
    public static boolean logIn(String email, String password) {
        if (AccountManager.getInstance().verifyLogIn(email, password)) {
            Session.openSession(email);

            return true;
        }

        return false;
    }

    /**
     * Attempt to create a new account with the given credentials and information.
     * If successful, create a new session for the user.
     *
     * @param email    User's email
     * @param name     User's name
     * @param password User's password
     * @return True if the account is created successfully, False otherwise
     */
    public static boolean register(String email, String name, String password) {
        if (AccountManager.getInstance().register(email, name, password)) {
            Session.openSession(email);

            return true;
        }

        return false;
    }

    /**
     * Deletes the user's account an data from the database.
     * If the user has a session open, the session is ended.
     *
     * @return True is the user's data is successfully deleted, False otherwise
     */
    public static boolean deleteUser() {
        String email = getEmail();
        boolean res = AccountManager.getInstance().deleteUser(email);

        try {
            if (res && Session.getSession().getEmail().toLowerCase().equals(email.toLowerCase())) {
                endSession();
            }
        } catch (NoSessionException e) {
            e.printStackTrace();
        }

        return res;
    }

    /**
     * Ends the current session.
     */
    public static void endSession() {
        Session.endSession();
    }

    /**
     * Sets the sorting method for the current session.
     *
     * @param method Sorting method to set
     * @return True if the method is valid, False otherwise
     */
    public static boolean setSortMethod(String method) {
        try {
            Session session = Session.getSession();
            switch (method) {
                case Controller.SORT_BY_NAME:
                    session.setSortMethod(Session.SORT_BY_NAME);

                    return true;
                case Controller.SORT_BY_DATE:
                    session.setSortMethod(Session.SORT_BY_DATE);

                    return true;
                case Controller.SORT_BY_DATE_ADDED:
                    session.setSortMethod(Session.SORT_BY_DATE_ADDED);

                    return true;
                default:

                    return false;
            }
        } catch (NoSessionException e) {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * @return The current user's email, or null if no session is active
     */
    public static String getEmail() {
        try {

            return Session.getSession().getEmail();
        } catch (NoSessionException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * @return The current user's name, or null if no session is active
     */
    public static String getName() {
        try {

            return Session.getSession().getName();
        } catch (NoSessionException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Obtains a JSON-formatted string containing a list of all the
     * user's deadline and its data.
     *
     * @return The string with the deadline list, or null if no session is active
     */
    public static String getAllDeadlines() {
        try {
            ArrayList<Deadline> deadlines = Session.getSession().getUserDeadlines();
            JSONArray jsonDeadlines = new JSONArray();
            for (Deadline deadline : deadlines) {
                JSONObject jsonDeadline = new JSONObject();
                jsonDeadline.put("id", deadline.getId());
                jsonDeadline.put("name", deadline.getName());
                jsonDeadline.put("dateTime", deadline.getDateTimeString());
                jsonDeadline.put("description", deadline.getDescription());
                jsonDeadline.put("notify", deadline.getNotify());
                jsonDeadline.put("dateTimeAdded", deadline.getDateTimeAddedString());
                jsonDeadline.put("color", deadline.getColor());
                jsonDeadlines.put(jsonDeadline);
            }

            return jsonDeadlines.toString();
        } catch (NoSessionException e) {
            e.printStackTrace();

            return null;
        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Obtains a JSON-formatted string containing a list of all the
     * user's events and its data.
     *
     * @return The string with the event list, or null if no session is active
     */
    public static String getAllEvents() {
        try {
            ArrayList<Event> events = Session.getSession().getUserEvents();
            JSONArray jsonEvents = new JSONArray();
            for (Event event : events) {
                JSONObject jsonDeadline = new JSONObject();
                jsonDeadline.put("id", event.getId());
                jsonDeadline.put("name", event.getName());
                jsonDeadline.put("dateTime", event.getDateTimeString());
                jsonDeadline.put("description", event.getDescription());
                jsonDeadline.put("notify", event.getNotify());
                jsonDeadline.put("dateTimeAdded", event.getDateTimeAddedString());
                jsonDeadline.put("color", event.getColor());
                jsonDeadline.put("duration", event.getDuration());
                jsonDeadline.put("location", event.getLocation());
                jsonEvents.put(jsonDeadline);
            }

            return jsonEvents.toString();
        } catch (NoSessionException e) {
            e.printStackTrace();

            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;

        }
    }

    /**
     * Obtains a JSON-formatted string containing the information of
     * a specific deadline identified by its id.
     *
     * @param id The desired deadline's id
     * @return The string with the deadline's data,
     * or null if the deadline was not found or if no session is active
     */
    public static String getDeadlineById(int id) {
        try {
            Deadline deadline = Session.getSession().findDeadlineById(id);
            JSONObject jsonDeadline = new JSONObject();
            jsonDeadline.put("id", deadline.getId());
            jsonDeadline.put("name", deadline.getName());
            jsonDeadline.put("dateTime", deadline.getDateTimeString());
            jsonDeadline.put("description", deadline.getDescription());
            jsonDeadline.put("notify", deadline.getNotify());
            jsonDeadline.put("dateTimeAdded", deadline.getDateTimeAddedString());
            jsonDeadline.put("color", deadline.getColor());

            return jsonDeadline.toString();
        } catch (NoSessionException e) {
            e.printStackTrace();

            return null;
        } catch (NullPointerException e) {
            e.printStackTrace();

            return null;
        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Obtains a JSON-formatted string containing the information of
     * a specific event identified by its id.
     *
     * @param id The desired event's id
     * @return The string with the event's data,
     * or null if the event was not found or if no session is active
     */
    public static String getEventById(int id) {
        try {
            Event event = Session.getSession().findEventById(id);
            JSONObject jsonEvent = new JSONObject();
            jsonEvent.put("id", event.getId());
            jsonEvent.put("name", event.getName());
            jsonEvent.put("dateTime", event.getDateTimeString());
            jsonEvent.put("description", event.getDescription());
            jsonEvent.put("notify", event.getNotify());
            jsonEvent.put("dateTimeAdded", event.getDateTimeAddedString());
            jsonEvent.put("color", event.getColor());
            jsonEvent.put("duration", event.getDuration());
            jsonEvent.put("location", event.getLocation());

            return jsonEvent.toString();
        } catch (NoSessionException e) {
            e.printStackTrace();

            return null;
        } catch (NullPointerException e) {
            e.printStackTrace();

            return null;
        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Adds a new deadline to the database containing the given data
     *
     * @param sJSONData JSON-formatted string with the deadline data
     * @return True if the deadline was successfully added, False otherwise
     */
    public static boolean addDeadline(String sJSONData) {
        try {
            HashMap<String, Object> data = parseJSONStringForDeadlineData(sJSONData);

            return Session.getSession().addDeadline(data);
        } catch (NoSessionException e) {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * Adds a new event to the database containing the given data
     *
     * @param sJSONData JSON-formatted string with the event data
     * @return True if the event was successfully added, False otherwise
     */
    public static boolean addEvent(String sJSONData) {
        try {
            HashMap<String, Object> data = parseJSONStringForEventData(sJSONData);

            return Session.getSession().addEvent(data);
        } catch (NoSessionException e) {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * Edits the deadline with the specified id so that its information
     * matches the given data.
     *
     * @param id        Id if the deadline to change
     * @param sJSONData JSON-formatted string with the new data for the deadline
     * @return True if the deadline is successfully edited, False otherwise
     */
    public static boolean editDeadline(int id, String sJSONData) {
        try {
            HashMap<String, Object> data = parseJSONStringForDeadlineData(sJSONData);

            return Session.getSession().editDeadline(id, data);
        } catch (NoSessionException e) {

            e.printStackTrace();

            return false;
        }
    }

    /**
     * Edits the event with the specified id so that its information
     * matches the given data.
     *
     * @param id        Id if the event to change
     * @param sJSONData JSON-formatted string with the new data for the event
     * @return True if the event is successfully edited, False otherwise
     */
    public static boolean editEvent(int id, String sJSONData) {
        try {
            HashMap<String, Object> data = parseJSONStringForEventData(sJSONData);

            return Session.getSession().editEvent(id, data);
        } catch (NoSessionException e) {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * Deletes the deadline with the specified id from the database.
     *
     * @param id Id of the deadline to be removed
     * @return True if the deadline was deleted successfully, False otherwise
     */
    public static boolean deleteDeadline(int id) {
        try {

            return Session.getSession().deleteDeadline(id);
        } catch (NoSessionException e) {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * Deletes the event with the specified id from the database.
     *
     * @param id Id of the event to be removed
     * @return True if the event was deleted successfully, False otherwise
     */
    public static boolean deleteEvent(int id) {
        try {

            return Session.getSession().deleteEvent(id);
        } catch (NoSessionException e) {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * Inverts the value "notify" for the deadline with the specified id both in the database.
     *
     * @param id Id of the deadline
     * @return New value of the "notify" property or False if the deadline was not found
     */
    public static boolean toggleDeadlineNotify(int id) {
        try {

            return Session.getSession().toggleDeadlineNotify(id);
        } catch (NoSessionException e) {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * Inverts the value "notify" for the event with the specified id both in the database.
     *
     * @param id Id of the event
     * @return New value of the "notify" property or False if the event was not found
     */
    public static boolean toggleEventNotify(int id) {
        try {

            return Session.getSession().toggleEventNotify(id);
        } catch (NoSessionException e) {
            e.printStackTrace();

            return false;
        }
    }

    /**
     * Parses a JSON-formatted string containing a deadline's information and
     * dumps the data into a HashMap.
     *
     * @param sJSONData The JSON-formatted string
     * @return The HashMap with the deadline data
     */
    private static HashMap<String, Object> parseJSONStringForDeadlineData(String sJSONData) {
        try {
            JSONObject jsonData = new JSONObject(sJSONData);
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", jsonData.getString("name"));
            data.put("dateTime", jsonData.getString("dateTime"));
            data.put("description", jsonData.getString("description"));
            data.put("notify", jsonData.getBoolean("notify"));
            data.put("dateTimeAdded", jsonData.getString("dateTimeAdded"));
            data.put("color", jsonData.getString("color"));

            return data;
        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Parses a JSON-formatted string containing an event's information and
     * dumps the data into a HashMap.
     *
     * @param sJSONData The JSON-formatted string
     * @return The HashMap with the event data
     */
    private static HashMap<String, Object> parseJSONStringForEventData(String sJSONData) {
        try {
            JSONObject jsonData = new JSONObject(sJSONData);
            HashMap<String, Object> data = new HashMap<>();
            data.put("name", jsonData.getString("name"));
            data.put("dateTime", jsonData.getString("dateTime"));
            data.put("description", jsonData.getString("description"));
            data.put("notify", jsonData.getBoolean("notify"));
            data.put("dateTimeAdded", jsonData.getString("dateTimeAdded"));
            data.put("color", jsonData.getString("color"));
            data.put("duration", jsonData.getInt("duration"));
            data.put("location", jsonData.getString("location"));

            return data;
        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }

}
