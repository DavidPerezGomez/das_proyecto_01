package com.example.proyecto_01.view.Activities.Events;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import com.example.proyecto_01.view.Activities.ThemeReadActivity;
import com.example.proyecto_01.view.Fragments.ShowEventFragment;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ShowEventActivity extends ThemeReadActivity {

    private int eventId;
    private Button buttonBack;
    private Button buttonEdit;
    private Button buttonCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_event);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.eventId = extras.getInt("id");
        }

        buttonBack = findViewById(R.id.buttonShowEventBack);
        buttonBack.setOnClickListener(v -> {
            Intent i = new Intent(this, ListEventsActivity.class);
            startActivity(i);
            finish();
        });

        buttonEdit = findViewById(R.id.buttonShowEventEdit);
        buttonEdit.setOnClickListener(v -> {
            Intent i = new Intent(this, EditEventActivity.class);
            i.putExtra("id", eventId);
            startActivity(i);
            finish();
        });

        buttonCalendar = findViewById(R.id.buttonShowEventInCalendar);
        buttonCalendar.setOnClickListener(v -> {
            try {
                // get the event date and time
                JSONObject jsonData = new JSONObject(Controller.getEventById(eventId));
                String sDateTime = jsonData.getString("dateTime");

                // parse the date/time into a Calendar object
                Calendar dateTime = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateTime.setTime(sdf.parse(sDateTime));

                // create the calendar intent and put the time of the event
                Intent i = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_CALENDAR);
                i.putExtra("beginTime", dateTime.getTimeInMillis());
                startActivity(i);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        ShowEventFragment showEventFragment = (ShowEventFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentShowEvent);
        showEventFragment.setId(eventId);
    }
}