package com.example.proyecto_01.view.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.example.proyecto_01.R;

public class ConfirmDialog extends DialogFragment {

    private ConfirmDialogListener listener;

    public interface ConfirmDialogListener {

        /**
         * Respond to the user confirming the account deletion
         */
        void delAccount();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        listener = (ConfirmDialogListener) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialogDelAccountTitle));
        builder.setMessage(getString(R.string.dialogDelAccountBody));
        builder.setPositiveButton(R.string.dialogDelAccountPositive, (dialog, which) -> listener.delAccount());
        builder.setNeutralButton(R.string.dialogDelAccountNegative, (dialog, which) -> {});
        return builder.create();
    }
}
