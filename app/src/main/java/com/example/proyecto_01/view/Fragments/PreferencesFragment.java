package com.example.proyecto_01.view.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import com.example.proyecto_01.R;

public class PreferencesFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    PreferenceFragmentListener listener;

    public interface PreferenceFragmentListener {

        /**
         * Respond to the user clicking on delete account.
         */
        void onClickDelAccount();

        /**
         * Respond to the user selecting a new language.
         */
        void onLanguageChanged();

        /**
         * Respond to the user selecting a new theme.
         */
        void onThemeChanged();
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);
        Preference delAccount = findPreference("delAccount");
        delAccount.setOnPreferenceClickListener(preference -> {
            listener.onClickDelAccount();
            return true;
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (PreferenceFragmentListener) context;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case "language":
                listener.onLanguageChanged();

                break;

            case "theme":
                listener.onThemeChanged();

                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}

