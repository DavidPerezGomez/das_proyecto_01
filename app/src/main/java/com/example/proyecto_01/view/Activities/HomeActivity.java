package com.example.proyecto_01.view.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Button;
import android.widget.TextView;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import com.example.proyecto_01.view.Activities.Deadlines.AddDeadlineActivity;
import com.example.proyecto_01.view.Activities.Deadlines.ListDeadlinesActivity;
import com.example.proyecto_01.view.Activities.Events.AddEventActivity;
import com.example.proyecto_01.view.Activities.Events.ListEventsActivity;
import com.example.proyecto_01.view.Activities.Login.LogInActivity;
import com.example.proyecto_01.view.Dialogs.AddDialog;

/**
 * Home activity, main menu
 */
public class HomeActivity extends ThemeReadActivity implements AddDialog.AddDialogListener {

    private final String addDialogTag = "addDialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // if there is not a session open, redirect to login
        if (!Controller.sessionIsOpen()) {
            Intent i = new Intent(this, LogInActivity.class);
            startActivity(i);
            finish();
        }

        updateGreeting();

        Button buttonPreferences = findViewById(R.id.buttonPreferences);
        buttonPreferences.setOnClickListener(v -> {
            Intent i = new Intent(this, PreferencesActivity.class);
            startActivity(i);
            finish();
        });

        Button buttonEvents = findViewById(R.id.buttonEvents);
        buttonEvents.setOnClickListener(v -> {
            Intent i = new Intent(this, ListEventsActivity.class);
            startActivity(i);
        });

        Button buttonDeadlines = findViewById(R.id.buttonDeadlines);
        buttonDeadlines.setOnClickListener(v -> {
            Intent i = new Intent(this, ListDeadlinesActivity.class);
            startActivity(i);
        });

        Button buttonLogOut = findViewById(R.id.buttonLogOut);
        buttonLogOut.setOnClickListener(v -> {
            Controller.endSession();
            Intent i = new Intent(this, LogInActivity.class);
            startActivity(i);
            finish();
        });

        Button buttonAdd = findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(v -> {
            AddDialog dialog = new AddDialog();
            dialog.show(getSupportFragmentManager(), addDialogTag);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateGreeting();
    }

    /**
     * Set the text on the greeting EditText according to the user and the preferences.
     */
    private void updateGreeting() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        TextView textViewGreeting = findViewById(R.id.textViewGreeting);
        if (preferences.getBoolean("useName", true)) {
            textViewGreeting.setText(String.format(getString(R.string.greeting), Controller.getName()));
        } else {
            textViewGreeting.setText(String.format(getString(R.string.greeting), Controller.getEmail()));
        }
    }

    @Override
    public void addDeadline() {
        Intent i = new Intent(this, AddDeadlineActivity.class);
        startActivity(i);
    }

    @Override
    public void addEvent() {
        Intent i = new Intent(this, AddEventActivity.class);
        startActivity(i);
    }

}
