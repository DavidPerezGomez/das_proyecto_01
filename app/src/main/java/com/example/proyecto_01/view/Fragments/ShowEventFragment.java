package com.example.proyecto_01.view.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

public class ShowEventFragment extends Fragment {

    private boolean viewCreated = false;

    private int eventId = -1;
    private TextView textViewTitle;
    private ScrollView scrollView;
    private TextView textViewName;
    private TextView textViewDescription;
    private TextView textViewDateTime;
    private TextView textViewLocation;
    private TextView textViewDuration;
    private TextView textViewNotify;

    public void setId(int id) {
        this.eventId = id;
        if (updateContents()) {
            showItems(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_show_event, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textViewTitle = getView().findViewById(R.id.textViewShowEventTitle);
        scrollView = getView().findViewById(R.id.scrollViewShowEvent);
        textViewName = getView().findViewById(R.id.textViewShowEventName);
        textViewDescription = getView().findViewById(R.id.textViewShowEventDescription);
        textViewDateTime = getView().findViewById(R.id.textViewShowEventDateTime);
        textViewNotify = getView().findViewById(R.id.textViewShowEventNotify);
        textViewLocation = getView().findViewById(R.id.textViewShowEventLocation);
        textViewDuration = getView().findViewById(R.id.textViewShowEventDuration);
        viewCreated = true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        updateContents();
    }

    private boolean updateContents() {
        if (viewCreated && eventId != -1) {
            try {
                String data = Controller.getEventById(eventId);
                JSONObject jsonData = new JSONObject(data);
                textViewName.setText(jsonData.getString("name"));
                textViewDescription.setText(jsonData.getString("description"));
                textViewDateTime.setText(String.format(getString(R.string.showEventDate), jsonData.getString("dateTime")));
                String notifyText;
                if (jsonData.getBoolean("notify")) {
                    notifyText = getString(R.string.showEventDoNotify);
                } else {
                    notifyText = getString(R.string.showEventDontNotify);
                }
                textViewLocation.setText(String.format(getString(R.string.showEventLocation), jsonData.getString("location")));
                textViewDuration.setText(String.format(getString(R.string.showEventDuration), jsonData.getString("duration")));
                textViewNotify.setText(notifyText);
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private void showItems(boolean show) {
        if (viewCreated) {
            if (show) {
                textViewTitle.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
            } else {
                textViewTitle.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
            }
        }
    }
}
