package com.example.proyecto_01.view.Activities.Deadlines;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.*;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import com.example.proyecto_01.view.Activities.Login.LogInActivity;
import com.example.proyecto_01.view.Activities.ThemeReadActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class AddDeadlineActivity extends ThemeReadActivity {

    private EditText editTextRed;
    private EditText editTextGreen;
    private EditText editTextBlue;
    private SeekBar seekBarRed;
    private SeekBar seekBarGreen;
    private SeekBar seekBarBlue;
    private ImageView imageViewColor;
    private EditText editTextName;
    private EditText editTextDate;
    private EditText editTextTime;
    private EditText editTextDescription;
    private Switch switchNotify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_deadline);

        if (!Controller.sessionIsOpen()) {
            Intent i = new Intent(this, LogInActivity.class);
            startActivity(i);
            finish();
        }

        editTextName = findViewById(R.id.editTextNewDeadlineName);
        editTextDate = findViewById(R.id.editTextNewDeadlineDate);
        editTextTime = findViewById(R.id.editTextNewDeadlineTime);
        editTextDescription = findViewById(R.id.editTextNewDeadlineDescription);

        switchNotify = findViewById(R.id.switchNewDeadlineNotify);

        Button buttonCancel = findViewById(R.id.buttonCancelDeadline);
        buttonCancel.setOnClickListener(v -> finish());
        Button buttonSave = findViewById(R.id.buttonSaveDeadline);
        buttonSave.setOnClickListener(v -> {
            if (submit()) {
                Toast.makeText(this, getString(R.string.toastDeadlineAdded), Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        imageViewColor = findViewById(R.id.imageViewDeadlineColor);

        seekBarRed = findViewById(R.id.seekBarDeadlineColorRed);
        seekBarGreen = findViewById(R.id.seekBarDeadlineColorGreen);
        seekBarBlue = findViewById(R.id.seekBarDeadlineColorBlue);

        editTextRed = findViewById(R.id.editTextDeadlineColorRed);
        editTextGreen = findViewById(R.id.editTextDeadlineColorGreen);
        editTextBlue = findViewById(R.id.editTextDeadlineColorBlue);

        SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    int value = seekBar.getProgress();
                    findColorText(seekBar).setText(Integer.toString(value));
                }
                updateImage();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };

        seekBarRed.setOnSeekBarChangeListener(seekBarChangeListener);
        seekBarGreen.setOnSeekBarChangeListener(seekBarChangeListener);
        seekBarBlue.setOnSeekBarChangeListener(seekBarChangeListener);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                int value = 0;
                if (!text.isEmpty()) {
                    try {
                        value = Integer.parseInt(text);
                        if (value > 255) {
                            s.replace(0, s.length(), "255");
                        } else if (value < 0) {
                            s.replace(0, s.length(), "0");
                        }
                    } catch (NumberFormatException e) {
                        s.replace(0, s.length(), "0");
                    }
                }

                EditText editText;
                int hash = s.hashCode();
                if (hash == editTextRed.getText().hashCode()) {
                    editText = editTextRed;
                } else if (hash == editTextGreen.getText().hashCode()) {
                    editText = editTextGreen;
                } else if (hash == editTextBlue.getText().hashCode()) {
                    editText = editTextBlue;
                } else {

                    return;
                }

                findSeekBar(editText).setProgress(value);
                updateImage();
            }
        };

        editTextRed.addTextChangedListener(textWatcher);
        editTextGreen.addTextChangedListener(textWatcher);
        editTextBlue.addTextChangedListener(textWatcher);

        editTextRed.setText("255");
        editTextGreen.setText("255");
        editTextBlue.setText("255");
    }

    /**
     * Given a SeekBar, finds the EditText that holds the value of the same color
     * as the SeekBar.
     *
     * @param seekBar The SeekBar
     * @return The EditText that corresponds to the SeekBar
     */
    private EditText findColorText(SeekBar seekBar) {
        if (seekBar == this.seekBarRed) {

            return this.editTextRed;
        } else if (seekBar == this.seekBarGreen) {

            return this.editTextGreen;
        } else if (seekBar == this.seekBarBlue) {

            return this.editTextBlue;
        }

        return null;
    }

    /**
     * Given an EditText, finds the EditText that controls the value of the same color
     * as the EditText.
     *
     * @param editText The EditText
     * @return The SeekBar that corresponds to the EditText
     */
    private SeekBar findSeekBar(EditText editText) {
        if (editText == this.editTextRed) {

            return this.seekBarRed;
        } else if (editText == this.editTextGreen) {

            return this.seekBarGreen;
        } else if (editText == this.editTextBlue) {

            return this.seekBarBlue;
        }

        return null;
    }

    /**
     * Updates the Image to show the color formed by the values of the SeekBars.
     */
    private void updateImage() {
        int r = seekBarRed.getProgress();
        int g = seekBarGreen.getProgress();
        int b = seekBarBlue.getProgress();
        this.imageViewColor.setBackgroundColor(Color.rgb(r, g, b));
    }

    /**
     * Check the data collected in the fields and try to add the new deadline
     * to the database.
     *
     * @return True if the data is valid and the deadline is added to the database, False otherwise
     */
    private boolean submit() {
        if (validateFields() && Controller.addDeadline(getJSONData())) {
            if (switchNotify.isChecked()) {
                String name = editTextName.getText().toString();
                notifyNotification(name);
            }
            Intent i = new Intent(this, ListDeadlinesActivity.class);
            startActivity(i);
            Toast.makeText(this, getString(R.string.toastDeadlineAdded), Toast.LENGTH_SHORT).show();
            finish();

            return true;
        }

        return false;
    }

    /**
     * Collects the data from the fields and dumps it into a JSON-formatter string.
     *
     * @return The string containing the data
     */
    public String getJSONData() {
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("name", editTextName.getText().toString());
            String date = editTextDate.getText().toString();
            String time = editTextTime.getText().toString();
            if (time.isEmpty()) {
                time = "00:00";
            }
            String dateTime = String.format("%s %s", date, time);
            jsonData.put("dateTime", dateTime);
            jsonData.put("description", editTextDescription.getText().toString());
            jsonData.put("notify", switchNotify.isChecked());
            Calendar calendarNow = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateTimeAdded = formatter.format(calendarNow.getTime());
            jsonData.put("dateTimeAdded", dateTimeAdded);
            int r = seekBarRed.getProgress();
            int g = seekBarGreen.getProgress();
            int b = seekBarBlue.getProgress();
            String hexColor = "#" + Integer.toHexString(Color.rgb(r, g, b)).substring(2);
            jsonData.put("color", hexColor);

            return jsonData.toString();
        } catch (JSONException e) {
            e.printStackTrace();

            return "";
        }
    }

    /**
     * Check whether the values in the fields are valid.
     *
     * @return True if all the fields contain valid values
     */
    public boolean validateFields() {
        View errorView = null;
        boolean ok = true;
        if (!editTextTime.getText().toString().isEmpty() && !timeFormatIsValid(editTextTime.getText().toString())) {
            editTextTime.setError(getString(R.string.errorTimeDeadline));
            errorView = editTextTime;
            ok = false;
        }

        if (!dateFormatIsValid(editTextDate.getText().toString())) {
            editTextDate.setError(getString(R.string.errorDateDeadline));
            errorView = editTextDate;
            ok = false;
        }

        if (editTextName.getText().toString().isEmpty()) {
            editTextName.setError(getString(R.string.errorNameDeadline));
            errorView = editTextName;
            ok = false;
        }

        if (errorView != null) {
            errorView.requestFocus();
        }

        return ok;
    }

    /**
     * Check whether the date's format is valid
     *
     * @param sDate String representing the date
     * @return True if the date's format is valid
     */
    private boolean dateFormatIsValid(String sDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        try {
            Date date = sdf.parse(sDate);

            return date != null;
        } catch (ParseException e) {

            return false;
        }
    }

    /**
     * Check whether the time's format is valid
     *
     * @param sTime String representing the time
     * @return True if the time's format is valid
     */
    private boolean timeFormatIsValid(String sTime) {
        String pattern = "(0?[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){1,2}";

        return Pattern.matches(pattern, sTime);
    }

    /**
     * Launch a notification saying that the user will be notified of the deadline
     * that is being, or has been created.
     *
     * @param name Name of the deadline
     */
    public void notifyNotification(String name) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "IdCanal");
        String content = String.format(getString(R.string.notificationDeadlineContent), name);
        builder.setSmallIcon(R.drawable.ic_notify_on)
                .setContentTitle(getString(R.string.notificationDeadlineTitle))
                .setContentText(content)
                .setVibrate(new long[]{0, 250})
                .setAutoCancel(true);
        notificationManager.notify(1, builder.build());
    }
}
