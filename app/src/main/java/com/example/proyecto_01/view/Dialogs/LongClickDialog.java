package com.example.proyecto_01.view.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.example.proyecto_01.R;

public class LongClickDialog extends DialogFragment {

    private LongClickDialogListener listener;

    private int id;

    public interface LongClickDialogListener {

        /**
         * Respond to the user clicking on the edit option.
         *
         * @param id Id of the list item that has been long clicked to activate this dialog
         */
        void dialogEdit(int id);

        /**
         * Respond to the user clicking on the delete option.
         *
         * @param id Id of the list item that has been long clicked to activate this dialog
         */
        void dialogDelete(int id);
    }


    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);
        this.id = args.getInt("id");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        listener = (LongClickDialogListener) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        CharSequence[] options = {getString(R.string.longClickEventEdit), getString(R.string.longClickEventDelete)};
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                default:
                case 0:
                    listener.dialogEdit(id);
                    break;

                case 1:
                    listener.dialogDelete(id);
                    break;
            }
        });
        return builder.create();
    }
}
