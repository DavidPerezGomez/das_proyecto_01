package com.example.proyecto_01.view.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import com.example.proyecto_01.view.Activities.Login.LogInActivity;
import com.example.proyecto_01.view.Dialogs.ConfirmDialog;
import com.example.proyecto_01.view.Fragments.PreferencesFragment;

import java.util.Locale;

public class PreferencesActivity extends ThemeReadActivity implements PreferencesFragment.PreferenceFragmentListener, ConfirmDialog.ConfirmDialogListener {

    private final String confirmDialogTag = "confirmDialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
    }

    @Override
    public void onClickDelAccount() {
        ConfirmDialog dialog = new ConfirmDialog();
        dialog.show(getSupportFragmentManager(), confirmDialogTag);
    }

    @Override
    public void onLanguageChanged() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String languageCode = preferences.getString("language", "es");
        Locale nuevaloc = new Locale(languageCode);
        Locale.setDefault(nuevaloc);
        Configuration config = new Configuration();
        config.locale = nuevaloc;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        recreate();
    }

    @Override
    public void onThemeChanged() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String themeCode = preferences.getString("theme", "deafault");
        switch (themeCode) {
            default:
            case "default":
                setTheme(R.style.AppTheme);

                break;
            case "alt":
                setTheme(R.style.AppThemeAlt);

                break;
        }

        recreate();
    }


    @Override
    public void delAccount() {
        Controller.deleteUser();
        Intent i = new Intent(this, LogInActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        finish();
    }
}
