package com.example.proyecto_01.view.RecyclerSupport;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.proyecto_01.R;

public class ListViewHolder extends RecyclerView.ViewHolder {

    private ListViewHolderListener listener;

    private TextView textViewName;
    private ImageView imageViewColor;
    private boolean notify;
    private Button buttonNotify;
    private Button buttonEdit;
    private int id;

    public interface ListViewHolderListener {

        /**
         * Respond to the user clicking on the item name.
         *
         * @param id Id of the item clicked
         */
        void onNameClick(int id);

        /**
         * Respond to the user holding click on the item name.
         *
         * @param id Id of the item clicked
         */
        void onNameLongClick(int id);

        /**
         * Respond to the user clicking on the edit icon.
         *
         * @param id Id of the item clicked
         */
        void onEditClick(int id);

        /**
         * Respond to the user clicking on the notification icon.
         *
         * @param id Id of the item clicked
         */
        boolean onNotifyClick(int id);
    }

    public ListViewHolder(@NonNull View itemView) {
        super(itemView);
        this.textViewName = itemView.findViewById(R.id.textViewItemName);
        this.imageViewColor = itemView.findViewById(R.id.imageViewItemColor);
        this.buttonNotify = itemView.findViewById(R.id.buttonItemNotify);
        this.buttonEdit = itemView.findViewById(R.id.buttonItemEdit);

        this.textViewName.setOnClickListener(v -> listener.onNameClick(id));

        this.textViewName.setOnLongClickListener(v -> {
            listener.onNameLongClick(id);
            return true;
        });

        this.buttonNotify.setOnClickListener(v -> {
            notify = listener.onNotifyClick(id);
            updateNotifyButton();
        });

        this.buttonEdit.setOnClickListener(v -> listener.onEditClick(id));
    }

    public void setListener(ListViewHolderListener listener) {
        this.listener = listener;
    }

    public void setName(String name) {
        this.textViewName.setText(name);
    }

    public void setColor(String sColor) {
        // "#rrggbb"
        int r = Integer.valueOf(sColor.substring(1, 3), 16);
        int g = Integer.valueOf(sColor.substring(3, 5), 16);
        int b = Integer.valueOf(sColor.substring(5, 7), 16);
        this.imageViewColor.setBackgroundColor(Color.rgb(r, g, b));
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
        updateNotifyButton();
    }

    private void updateNotifyButton() {
        if (this.notify) {
            this.buttonNotify.setBackgroundResource(R.drawable.ic_notify_on);
        } else {
            this.buttonNotify.setBackgroundResource(R.drawable.ic_notify_off);
        }
    }

    public void setId(int id) {
        this.id = id;
    }

}
