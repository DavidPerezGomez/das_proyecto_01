package com.example.proyecto_01.view.Activities.Events;

import android.content.Intent;
import android.os.Bundle;
import android.widget.*;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

public class EditEventActivity extends AddEventActivity {

    private int eventId;
    private EditText editTextName;
    private Switch switchNotify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.eventId = extras.getInt("id");
        }

        String deadlineData = Controller.getEventById(eventId);
        try {
            JSONObject jsonData = new JSONObject(deadlineData);
            System.out.println(deadlineData);

            TextView textViewTitle = findViewById(R.id.textViewNewEventTitle);
            textViewTitle.setText(getString(R.string.titleEditEvent));

            editTextName = findViewById(R.id.editTextNewEventName);
            editTextName.setText(jsonData.getString("name"));

            EditText editTextDate = findViewById(R.id.editTextNewEventDate);
            EditText editTextTime = findViewById(R.id.editTextNewEventTime);
            String[] dateTime = jsonData.getString("dateTime").split(" ");
            String date = dateTime[0];
            String time = dateTime[1];
            editTextDate.setText(date);
            editTextTime.setText(time);

            EditText editTextDescription = findViewById(R.id.editTextNewEventDescription);
            editTextDescription.setText(jsonData.getString("description"));

            EditText editTextDuration = findViewById(R.id.editTextNewEventDuration);
            editTextDuration.setText(jsonData.getString("duration"));

            EditText editTextLocation = findViewById(R.id.editTextNewEventLocation);
            editTextLocation.setText(jsonData.getString("location"));

            switchNotify = findViewById(R.id.switchNewEventNotify);
            switchNotify.setChecked(jsonData.getBoolean("notify"));

            SeekBar seekBarRed = findViewById(R.id.seekBarEventColorRed);
            SeekBar seekBarGreen = findViewById(R.id.seekBarEventColorGreen);
            SeekBar seekBarBlue = findViewById(R.id.seekBarEventColorBlue);
            String sColor = jsonData.getString("color");
            int r = Integer.valueOf(sColor.substring(1, 3), 16);
            int g = Integer.valueOf(sColor.substring(3, 5), 16);
            int b = Integer.valueOf(sColor.substring(5, 7), 16);
            seekBarRed.setProgress(r);
            seekBarGreen.setProgress(g);
            seekBarBlue.setProgress(b);

            EditText editTextRed = findViewById(R.id.editTextEventColorRed);
            EditText editTextGreen = findViewById(R.id.editTextEventColorGreen);
            EditText editTextBlue = findViewById(R.id.editTextEventColorBlue);
            editTextRed.setText(Integer.toString(r));
            editTextGreen.setText(Integer.toString(g));
            editTextBlue.setText(Integer.toString(b));

            Button buttonCancel = findViewById(R.id.buttonCancelEvent);
            buttonCancel.setOnClickListener(v -> {
                Intent i = new Intent(this, ListEventsActivity.class);
                startActivity(i);
                finish();
            });

            Button buttonSave = findViewById(R.id.buttonSaveEvent);
            buttonSave.setOnClickListener(v -> {
                if (submit()) {
                    Toast.makeText(this, getString(R.string.toastEventEdited), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private boolean submit() {
        if (validateFields() && Controller.editEvent(eventId, getJSONData())) {
            if (switchNotify.isChecked()) {
                String name = editTextName.getText().toString();
                notifyNotification(name);
            }
            finish();

            return true;
        }

        return false;
    }

}
