package com.example.proyecto_01.view.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.proyecto_01.R;
import com.example.proyecto_01.view.RecyclerSupport.ListAdapter;

public class ItemListFragment extends Fragment implements ListAdapter.ListAdapterListener {

    private ItemListFragmentListener listener;

    private RecyclerView recyclerViewList;
    private String[] names;
    private String[] colors;
    private boolean[] notifications;
    private int[] ids;

    public interface ItemListFragmentListener {

        /**
         * Respond to the user clicking on the item name.
         *
         * @param id Id of the item clicked
         */
        void onItemNameClick(int id);

        /**
         * Respond to the user holding click on the item name.
         *
         * @param id Id of the item clicked
         */
        void onItemNameLongClick(int id);

        /**
         * Respond to the user clicking on the edit icon.
         *
         * @param id Id of the item clicked
         */
        void onItemEditClick(int id);

        /**
         * Respond to the user clicking on the notification icon.
         *
         * @param id Id of the item clicked
         */
        boolean onItemNotifyClick(int id);
    }

    public void setContents(int[] ids, String[] names, String[] colors, boolean[] notifications) {
        this.ids = ids;
        this.names = names;
        this.colors = colors;
        this.notifications = notifications;
        updateList();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_item_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        updateList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ItemListFragmentListener) context;
    }

    private void updateList() {
        this.recyclerViewList = getView().findViewById(R.id.recyclerViewList);
        ListAdapter adapter = new ListAdapter(this, names, colors, notifications, ids);
        recyclerViewList.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewList.setLayoutManager(layoutManager);

        DividerItemDecoration dividerDecorator = new DividerItemDecoration(recyclerViewList.getContext(), LinearLayoutManager.VERTICAL);
        recyclerViewList.addItemDecoration(dividerDecorator);
    }

    @Override
    public void onItemNameClick(int id) {
        listener.onItemNameClick(id);
    }

    @Override
    public void onItemNameLongClick(int id) {
        listener.onItemNameLongClick(id);
    }

    @Override
    public void onItemEditClick(int id) {
        listener.onItemEditClick(id);
    }

    @Override
    public boolean onItemNotifyClick(int id) {
        return listener.onItemNotifyClick(id);
    }
}
