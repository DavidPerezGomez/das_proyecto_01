package com.example.proyecto_01.view.Activities.Login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import com.example.proyecto_01.view.Activities.HomeActivity;
import com.example.proyecto_01.view.Activities.ThemeReadActivity;

public class RegisterActivity extends ThemeReadActivity {

    private EditText editTextEmail;
    private EditText editTextName;
    private EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Controller.endSession();

        editTextEmail = findViewById(R.id.editTextRegisterEmail);
        editTextName = findViewById(R.id.editTextRegisterName);

        editTextPassword = findViewById(R.id.editTextRegisterPassword);
        editTextPassword.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptRegister();
                return true;
            }
            return false;
        });

        Button buttonRegister = findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(view -> attemptRegister());

        Button buttonBack = findViewById(R.id.buttonRegisterBack);
        buttonBack.setOnClickListener( view -> onBackPressed());
    }

    private void attemptRegister() {
        // Reset errors.
        editTextEmail.setError(null);
        editTextPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = editTextEmail.getText().toString();
        String name = editTextName.getText().toString();
        String password = editTextPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            editTextPassword.setError(getString(R.string.errorInvalidPassword));
            focusView = editTextPassword;
            cancel = true;
        }

        // Check for a valid name.
        if (TextUtils.isEmpty(name)) {
            editTextName.setError(getString(R.string.errorFieldRequired));
            focusView = editTextName;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError(getString(R.string.errorFieldRequired));
            focusView = editTextEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            editTextEmail.setError(getString(R.string.errorInvalidEmail));
            focusView = editTextEmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            if (Controller.register(email.toLowerCase(), name, password)) {
                Intent i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
            } else {
                editTextEmail.setError(getString(R.string.errorDuplicateEmail));
                editTextEmail.requestFocus();
            }
        }

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, LogInActivity.class);
        startActivity(i);
        finish();
    }
}
