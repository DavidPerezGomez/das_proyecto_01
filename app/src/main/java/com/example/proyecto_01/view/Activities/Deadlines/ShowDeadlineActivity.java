package com.example.proyecto_01.view.Activities.Deadlines;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import com.example.proyecto_01.view.Activities.ThemeReadActivity;
import com.example.proyecto_01.view.Fragments.ShowDeadlineFragment;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ShowDeadlineActivity extends ThemeReadActivity {

    private int deadlineId;
    private Button buttonBack;
    private Button buttonEdit;
    private Button buttonCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_deadline);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.deadlineId = extras.getInt("id");
        }

        buttonBack = findViewById(R.id.buttonShowDeadlineBack);
        buttonBack.setOnClickListener(v -> {
            Intent i = new Intent(this, ListDeadlinesActivity.class);
            startActivity(i);
            finish();
        });

        buttonEdit = findViewById(R.id.buttonShowDeadlineEdit);
        buttonEdit.setOnClickListener(v -> {
            Intent i = new Intent(this, EditDeadlineActivity.class);
            i.putExtra("id", deadlineId);
            startActivity(i);
            finish();
        });

        buttonCalendar = findViewById(R.id.buttonShowDeadlineInCalendar);
        buttonCalendar.setOnClickListener(v -> {
            try {
                // get the deadline date and time
                JSONObject jsonData = new JSONObject(Controller.getDeadlineById(deadlineId));
                String sDateTime = jsonData.getString("dateTime");

                // parse the date/time into a Calendar object
                Calendar dateTime = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateTime.setTime(sdf.parse(sDateTime));

                // create the calendar intent and put the time of the deadline
                Intent i = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_CALENDAR);
                i.putExtra("beginTime", dateTime.getTimeInMillis());
                startActivity(i);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        ShowDeadlineFragment showDeadlineFragment = (ShowDeadlineFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentShowDeadline);
        showDeadlineFragment.setId(deadlineId);
    }
}
