package com.example.proyecto_01.view.Activities.Deadlines;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import com.example.proyecto_01.view.Activities.HomeActivity;
import com.example.proyecto_01.view.Activities.Login.LogInActivity;
import com.example.proyecto_01.view.Activities.ThemeReadActivity;
import com.example.proyecto_01.view.Dialogs.LongClickDialog;
import com.example.proyecto_01.view.Fragments.ItemListFragment;
import com.example.proyecto_01.view.Fragments.ShowDeadlineFragment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListDeadlinesActivity extends ThemeReadActivity implements ItemListFragment.ItemListFragmentListener, LongClickDialog.LongClickDialogListener {

    private final String longClickDialogTag = "longClickDialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_deadlines);

        if (!Controller.sessionIsOpen()) {
            Intent i = new Intent(this, LogInActivity.class);
            startActivity(i);
            finish();
        }

        TextView textViewTitle = findViewById(R.id.textViewListTitle);
        textViewTitle.setText(R.string.listTitleDeadlines);

        Button buttonAddEvent = findViewById(R.id.buttonAddDeadline);
        buttonAddEvent.setOnClickListener(v -> {
            Intent i = new Intent(this, AddDeadlineActivity.class);
            startActivity(i);
        });

        Button buttonBack = findViewById(R.id.buttonListDeadlinesBack);
        buttonBack.setOnClickListener(v -> {
            Intent i = new Intent(this, HomeActivity.class);
            startActivity(i);
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList() {
        int[] ids = {};
        String[] names = {};
        String[] colors = {};
        boolean[] notifications = {};

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String sortMethodCode = preferences.getString("sortMethod", "date");
        String sortMethod;
        switch (sortMethodCode) {
            case "name":
                sortMethod = Controller.SORT_BY_NAME;
                break;
            default:
            case "date":
                sortMethod = Controller.SORT_BY_DATE;
                break;
            case "dateAdded":
                sortMethod = Controller.SORT_BY_DATE_ADDED;
                break;
        }
        Controller.setSortMethod(sortMethod);

        try {
            JSONArray deadlines = new JSONArray(Controller.getAllDeadlines());
            int length = deadlines.length();
            ids = new int[length];
            names = new String[length];
            colors = new String[length];
            notifications = new boolean[length];
            for (int i = 0; i < length; i++) {
                JSONObject deadline = (JSONObject) deadlines.get(i);
                ids[i] = deadline.getInt("id");
                names[i] = deadline.getString("name");
                colors[i] = deadline.getString("color");
                notifications[i] = deadline.getBoolean("notify");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ItemListFragment listFragment = (ItemListFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentDeadlineList);
        listFragment.setContents(ids, names, colors, notifications);
    }

    private void notifyNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "IdCanal");
        builder.setSmallIcon(R.drawable.ic_notify_on)
                .setContentTitle(getString(R.string.notificationDeadlineUpdated))
                .setVibrate(new long[]{0, 250})
                .setAutoCancel(true);
        notificationManager.notify(1, builder.build());
    }

    private void toEditDeadline(int id) {
        Intent i = new Intent(this, EditDeadlineActivity.class);
        i.putExtra("id", id);
        startActivity(i);
    }

    @Override
    public void onItemNameClick(int id) {
        ShowDeadlineFragment showFragment = (ShowDeadlineFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentDeadlineListResult);
        if (showFragment == null) {
            Intent i = new Intent(this, ShowDeadlineActivity.class);
            i.putExtra("id", id);
            startActivity(i);
        } else {
            showFragment.setId(id);
        }
    }

    @Override
    public void onItemNameLongClick(int id) {
        LongClickDialog dialog = new LongClickDialog();
        Bundle args = new Bundle();
        args.putInt("id", id);
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), longClickDialogTag);
    }

    @Override
    public void onItemEditClick(int id) {
        toEditDeadline(id);
    }

    @Override
    public boolean onItemNotifyClick(int id) {
        boolean res = Controller.toggleDeadlineNotify(id);
        ShowDeadlineFragment showFragment = (ShowDeadlineFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentDeadlineListResult);
        if (showFragment != null) {
            showFragment.setId(id);
        }
        if (res) {
            notifyNotification();
        }
        return res;
    }

    @Override
    public void dialogEdit(int id) {
        toEditDeadline(id);
    }

    @Override
    public void dialogDelete(int id) {
        if (Controller.deleteDeadline(id)) {
            updateList();
            Toast.makeText(this, getString(R.string.toastDeadlineDeleted), Toast.LENGTH_SHORT).show();
        }
    }

}