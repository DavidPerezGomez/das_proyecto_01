package com.example.proyecto_01.view.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.example.proyecto_01.R;

public class AddDialog extends DialogFragment {

    private AddDialogListener listener;

    public interface AddDialogListener {

        /**
         * Respond to the user clicking on "add deadline"
         */
        void addDeadline();

        /**
         * Respond to the user clicking on "add event"
         */
        void addEvent();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        listener = (AddDialogListener) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        CharSequence[] options = {getString(R.string.dialogAddDeadline), getString(R.string.dialogAddEvent)};
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                default:
                case 0:
                    listener.addDeadline();
                    break;

                case 1:
                    listener.addEvent();
                    break;
            }
        });
        return builder.create();
    }
}
