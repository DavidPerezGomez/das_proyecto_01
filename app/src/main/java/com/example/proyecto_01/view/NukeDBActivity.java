package com.example.proyecto_01.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;

public class NukeDBActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuke_db);

        TextView editTextResult = findViewById(R.id.textViewResult);
        if (Controller.nukeDB()) {
            editTextResult.setText("nuked");
        } else {
            editTextResult.setText("failed");
        }

    }
}
