package com.example.proyecto_01.view.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import com.example.proyecto_01.R;

/**
 * Class to read the theme set on preferences and apply it before creating the view.
 * Outside of that, it behaves exactly like a regular activity.
 * Meant to be used a parent.
 */
public class ThemeReadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //load the preferences and read the theme property
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String theme = preferences.getString("theme", "default");

        // apply the theme before calling super.onCreate()
        switch(theme) {
            default:
            case "default":
                setTheme(R.style.AppTheme);
                break;
            case "alt":
                setTheme(R.style.AppThemeAlt);
                break;
        }

        super.onCreate(savedInstanceState);
    }

}
