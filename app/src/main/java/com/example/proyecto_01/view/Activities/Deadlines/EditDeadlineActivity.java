package com.example.proyecto_01.view.Activities.Deadlines;

import android.content.Intent;
import android.os.Bundle;
import android.widget.*;
import com.example.proyecto_01.R;
import com.example.proyecto_01.controller.Controller;
import org.json.JSONException;
import org.json.JSONObject;

public class EditDeadlineActivity extends AddDeadlineActivity {

    private int deadlineId;
    private EditText editTextName;
    private Switch switchNotify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.deadlineId = extras.getInt("id");
        }

        String deadlineData = Controller.getDeadlineById(deadlineId);
        try {
            JSONObject jsonData = new JSONObject(deadlineData);
            System.out.println(deadlineData);

            TextView textViewTitle = findViewById(R.id.textViewNewDeadlineTitle);
            textViewTitle.setText(getString(R.string.titleEditDeadline));

            editTextName = findViewById(R.id.editTextNewDeadlineName);
            editTextName.setText(jsonData.getString("name"));

            EditText editTextDate = findViewById(R.id.editTextNewDeadlineDate);
            EditText editTextTime = findViewById(R.id.editTextNewDeadlineTime);
            String[] dateTime = jsonData.getString("dateTime").split(" ");
            String date = dateTime[0];
            String time = dateTime[1];
            editTextDate.setText(date);
            editTextTime.setText(time);

            EditText editTextDescription = findViewById(R.id.editTextNewDeadlineDescription);
            editTextDescription.setText(jsonData.getString("description"));

            switchNotify = findViewById(R.id.switchNewDeadlineNotify);
            switchNotify.setChecked(jsonData.getBoolean("notify"));

            SeekBar seekBarRed = findViewById(R.id.seekBarDeadlineColorRed);
            SeekBar seekBarGreen = findViewById(R.id.seekBarDeadlineColorGreen);
            SeekBar seekBarBlue = findViewById(R.id.seekBarDeadlineColorBlue);
            String sColor = jsonData.getString("color");
            int r = Integer.valueOf(sColor.substring(1, 3), 16);
            int g = Integer.valueOf(sColor.substring(3, 5), 16);
            int b = Integer.valueOf(sColor.substring(5, 7), 16);
            seekBarRed.setProgress(r);
            seekBarGreen.setProgress(g);
            seekBarBlue.setProgress(b);

            EditText editTextRed = findViewById(R.id.editTextDeadlineColorRed);
            EditText editTextGreen = findViewById(R.id.editTextDeadlineColorGreen);
            EditText editTextBlue = findViewById(R.id.editTextDeadlineColorBlue);
            editTextRed.setText(Integer.toString(r));
            editTextGreen.setText(Integer.toString(g));
            editTextBlue.setText(Integer.toString(b));

            Button buttonCancel = findViewById(R.id.buttonCancelDeadline);
            buttonCancel.setOnClickListener(v -> {
                Intent i = new Intent(this, ListDeadlinesActivity.class);
                startActivity(i);
                finish();
            });

            Button buttonSave = findViewById(R.id.buttonSaveDeadline);
            buttonSave.setOnClickListener(v -> {
                if (submit()) {
                    Toast.makeText(this, getString(R.string.toastDeadlineEdited), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private boolean submit() {
        if (validateFields() && Controller.editDeadline(deadlineId, getJSONData())) {
            if (switchNotify.isChecked()) {
                String name = editTextName.getText().toString();
                notifyNotification(name);
            }
            finish();

            return true;
        }

        return false;
    }

}
