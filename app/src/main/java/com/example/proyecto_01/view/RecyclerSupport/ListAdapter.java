package com.example.proyecto_01.view.RecyclerSupport;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.proyecto_01.R;

public class ListAdapter extends RecyclerView.Adapter<ListViewHolder> implements ListViewHolder.ListViewHolderListener {

    private ListAdapterListener listener;

    private String[] names;
    private String[] colors;
    private boolean[] notifications;
    private int[] ids;

    public interface ListAdapterListener {

        /**
         * Respond to the user clicking on the item name.
         *
         * @param id Id of the item clicked
         */
        void onItemNameClick(int id);

        /**
         * Respond to the user holding click on the item name.
         *
         * @param id Id of the item clicked
         */
        void onItemNameLongClick(int id);

        /**
         * Respond to the user clicking on the edit icon.
         *
         * @param id Id of the item clicked
         */
        void onItemEditClick(int id);

        /**
         * Respond to the user clicking on the notification icon.
         *
         * @param id Id of the item clicked
         */
        boolean onItemNotifyClick(int id);
    }

    @Override
    public void onNameClick(int id) {
        listener.onItemNameClick(id);
    }

    @Override
    public void onNameLongClick(int id) {
        listener.onItemNameLongClick(id);
    }

    @Override
    public void onEditClick(int id) {
        listener.onItemEditClick(id);

    }

    @Override
    public boolean onNotifyClick(int id) {
        return listener.onItemNotifyClick(id);
    }

    public ListAdapter(ListAdapterListener listener, String[] names, String[] colors, boolean[] notifications, int[] ids) {
        super();
        this.listener = listener;
        this.names = names;
        this.colors = colors;
        this.notifications = notifications;
        this.ids = ids;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemLayout = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new ListViewHolder(itemLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
        listViewHolder.setListener(this);
        listViewHolder.setName(this.names[i]);
        listViewHolder.setColor(this.colors[i]);
        listViewHolder.setNotify(this.notifications[i]);
        listViewHolder.setId(this.ids[i]);
    }

    @Override
    public int getItemCount() {
        return this.names.length;
    }
}
