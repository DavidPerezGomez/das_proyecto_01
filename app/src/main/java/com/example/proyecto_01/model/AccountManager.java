package com.example.proyecto_01.model;

import com.example.proyecto_01.EventReminder;

import java.util.HashMap;

public class AccountManager {

    private static final AccountManager theManager = new AccountManager();

    private DBManager dbManager;

    public static AccountManager getInstance() {
        return theManager;
    }

    private AccountManager() {
        dbManager = new DBManager(EventReminder.getContext(), "event_reminder", null, 1);
    }

    /**
     * Check if the credentials correspond to a user stored in the database.
     *
     * @param email    The user's email
     * @param password The user's password
     * @return True if the credentials are valid, False otherwise
     */
    public boolean verifyLogIn(String email, String password) {
        HashMap<String, String> credentials = dbManager.getCredetials(email);
        if (credentials != null) {
            // user exists: check for password
            // TODO get salt and hash the password
            String password_hash = hashPassword(password, "");

            String real_hash = credentials.get("password_hash");

            return real_hash.equals(password_hash);
        }

        return false;
    }

    /**
     * Register a new user with the given information into the database.
     *
     * @param email    The user's email
     * @param name     The user's name
     * @param password The user's password
     * @return True if the user is successfully entered into the database, False otherwise
     */
    public boolean register(String email, String name, String password) {
        // TODO get salt and hash the password
        String password_hash = hashPassword(password, "");

        return dbManager.insertUser(email, name, password_hash);
    }

    /**
     * Removes the user with the given email from the database.
     *
     * @param email The user's email
     * @return True if the user is successfully removed from the database, False otherwise
     */
    public boolean deleteUser(String email) {
        return dbManager.deleteUser(email);
    }

    private String hashPassword(String password, String salt) {
        // TODO hash here
        // or don't
        // i'm a comment, not a cop
        return password;
    }
}
