package com.example.proyecto_01.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class DBManager extends SQLiteOpenHelper {

    public DBManager(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            StringBuilder sql = new StringBuilder();
            InputStream is = getClass().getClassLoader().getResourceAsStream("res/raw/structure.sql");
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                sql.append(line);
            }
            br.close();
            String[] statements = sql.toString().split(";");
            for (String statement : statements) {
                statement += ";";
                System.out.println(statement);
                db.execSQL(statement);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public HashMap<String, String> getCredetials(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = String.format("SELECT * FROM user WHERE email='%s'", email);
        Cursor c = db.rawQuery(sql, null);
        HashMap<String, String> credentials = null;
        if (c.moveToNext()) {
            credentials = new HashMap<>();
            credentials.put("email", c.getString(c.getColumnIndex("email")));
            credentials.put("password_hash", c.getString(c.getColumnIndex("password_hash")));
        }
        c.close();
        db.close();
        return credentials;
    }

    public boolean insertUser(String email, String name, String password_hash) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("email", email);
        values.put("name", name);
        values.put("password_hash", password_hash);
        try {
            long res = db.insert("user", null, values);
            db.close();
            return res != -1;
        } catch (SQLiteConstraintException e) {
            db.close();
            return false;
        }
    }

    public boolean deleteUser(String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        int res = db.delete("user", String.format("email='%s'", email), null);
        db.delete("deadline", String.format("user_email='%s'", email), null);
        db.delete("event", String.format("user_email='%s'", email), null);
        db.close();
        return res > 0;
    }

    public User getUserByEmail(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = String.format("SELECT * FROM user WHERE email='%s'", email);
        Cursor c = db.rawQuery(sql, null);
        User user = null;
        if (c.moveToNext()) {
            String name = c.getString(c.getColumnIndex("name"));
            user = new User(email, name);
        }
        c.close();
        db.close();
        return user;
    }

    public ArrayList<Deadline> getDeadlinesByEmail(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = String.format("SELECT rowid, * FROM deadline WHERE user_email='%s'", email);
        Cursor c = db.rawQuery(sql, null);
        ArrayList<Deadline> deadlines = new ArrayList<>(c.getCount());
        while (c.moveToNext()) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("id", c.getInt(c.getColumnIndex("rowid")));
            data.put("name", c.getString(c.getColumnIndex("name")));
            data.put("dateTime", c.getString(c.getColumnIndex("date_time")));
            data.put("description", c.getString(c.getColumnIndex("description")));
            data.put("notify", c.getInt(c.getColumnIndex("notify")) == 1);
            data.put("dateTimeAdded", c.getString(c.getColumnIndex("date_added")));
            data.put("color", c.getString(c.getColumnIndex("color")));
            deadlines.add(new Deadline(data));
        }
        c.close();
        db.close();
        return deadlines;
    }

    public ArrayList<Event> getEventsByEmail(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = String.format("SELECT rowid, * FROM event WHERE user_email='%s'", email);
        Cursor c = db.rawQuery(sql, null);
        ArrayList<Event> events = new ArrayList<>(c.getCount());
        while (c.moveToNext()) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("id", c.getInt(c.getColumnIndex("rowid")));
            data.put("name", c.getString(c.getColumnIndex("name")));
            data.put("dateTime", c.getString(c.getColumnIndex("date_time")));
            data.put("description", c.getString(c.getColumnIndex("description")));
            data.put("notify", c.getInt(c.getColumnIndex("notify")) == 1);
            data.put("duration", c.getInt(c.getColumnIndex("duration")));
            data.put("location", c.getString(c.getColumnIndex("location")));
            data.put("dateTimeAdded", c.getString(c.getColumnIndex("date_added")));
            data.put("color", c.getString(c.getColumnIndex("color")));
            events.add(new Event(data));
        }
        c.close();
        db.close();
        return events;
    }

    public boolean insertDeadline(HashMap<String, Object> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_email", (String) data.get("email"));
        values.put("name", (String) data.get("name"));
        values.put("date_time", (String) data.get("dateTime"));
        values.put("description", (String) data.get("description"));
        values.put("notify", ((boolean) data.get("notify")) ? 1 : 0);
        values.put("date_added", (String) data.get("dateTimeAdded"));
        values.put("color", (String) data.get("color"));
        try {
            long res = db.insert("deadline", null, values);
            db.close();
            return res != -1;
        } catch (SQLiteConstraintException e) {
            db.close();
            return false;
        }
    }

    public boolean insertEvent(HashMap<String, Object> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_email", (String) data.get("email"));
        values.put("name", (String) data.get("name"));
        values.put("date_time", (String) data.get("dateTime"));
        values.put("description", (String) data.get("description"));
        values.put("notify", ((boolean) data.get("notify")) ? 1 : 0);
        values.put("duration", (int) data.get("duration"));
        values.put("location", (String) data.get("location"));
        values.put("date_added", (String) data.get("dateTimeAdded"));
        values.put("color", (String) data.get("color"));
        try {
            long res = db.insert("event", null, values);
            db.close();
            return res != -1;
        } catch (SQLiteConstraintException e) {
            db.close();
            return false;
        }
    }

    public boolean updateDeadline(int id, HashMap<String, Object> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_email", (String) data.get("email"));
        values.put("name", (String) data.get("name"));
        values.put("date_time", (String) data.get("dateTime"));
        values.put("description", (String) data.get("description"));
        values.put("notify", ((boolean) data.get("notify")) ? 1 : 0);
        values.put("date_added", (String) data.get("dateTimeAdded"));
        values.put("color", (String) data.get("color"));
        try {
            long res = db.update("deadline", values, String.format("rowid=%d", id), null);
            db.close();
            return res != -1;
        } catch (SQLiteConstraintException e) {
            db.close();
            return false;
        }
    }

    public boolean updateEvent(int id, HashMap<String, Object> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_email", (String) data.get("email"));
        values.put("name", (String) data.get("name"));
        values.put("date_time", (String) data.get("dateTime"));
        values.put("description", (String) data.get("description"));
        values.put("notify", ((boolean) data.get("notify")) ? 1 : 0);
        values.put("duration", (int) data.get("duration"));
        values.put("location", (String) data.get("location"));
        values.put("date_added", (String) data.get("dateTimeAdded"));
        values.put("color", (String) data.get("color"));
        try {
            long res = db.update("event", values, String.format("rowid=%d", id), null);
            db.close();
            return res != -1;
        } catch (SQLiteConstraintException e) {
            db.close();
            return false;
        }
    }

    public boolean deleteDeadline(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        long res = db.delete("deadline", String.format("rowid=%d", id), null);
        db.close();
        return res != -1;
    }

    public boolean deleteEvent(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        long res = db.delete("event", String.format("rowid=%d", id), null);
        db.close();
        return res != -1;
    }

    public boolean setDeadlineNotify(int id, boolean notify) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("notify", (notify ? 1 : 0));
        try {
            long res = db.update("deadline", value, String.format("rowid=%d", id), null);
            db.close();
            return res != -1;
        } catch (SQLiteConstraintException e) {
            db.close();
            return false;
        }
    }

    public boolean setEventNotify(int id, boolean notify) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put("notify", (notify ? 1 : 0));
        try {
            long res = db.update("event", value, String.format("rowid=%d", id), null);
            db.close();
            return res != -1;
        } catch (SQLiteConstraintException e) {
            db.close();
            return false;
        }
    }

    public int getNextDeadlineId() {
        return getNextId("deadline");
    }

    public int getNextEventId() {
        return getNextId("event");
    }

    private int getNextId(String table) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = String.format("SELECT MAX(rowid) as 'id' FROM '%s'", table);
        Cursor c = db.rawQuery(sql, null);
        int id = 0;
        if (c.moveToNext()) {
            id = c.getInt(c.getColumnIndex("id"));
        }
        c.close();
        db.close();
        return id + 1;
    }

}
