package com.example.proyecto_01.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class Deadline {

    private int id;
    private String name;
    private Calendar dateTime;
    private String description;
    private boolean notify;
    private User user;
    private Calendar dateTimeAdded;
    private String color;

    public Deadline(HashMap<String, Object> data) {
        this.update(data);
    }

    /**
     * Updates the information so that it matches that of the given HashMap
     *
     * @param data HashMap with the new data
     */
    public void update(HashMap<String, Object> data) {
        if (data != null) {
            setId((int) data.get("id"));
            setName((String) data.get("name"));
            setDateTime((String) data.get("dateTime"));
            setDescription((String) data.get("description"));
            setNotify((boolean) data.get("notify"));
            setUser((User) data.get("user"));
            setDateTimeAdded((String) data.get("dateTimeAdded"));
            setColor((String) data.get("color"));
        }
    }

    /**
     * Formats the required information and dumps it into a HashMap.
     *
     * @return The HashMap with the instance's data
     */
    public HashMap<String, Object> getDataForDB() {
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", getName());
        data.put("dateTime", getDateTimeString());
        data.put("description", getDescription());
        data.put("notify", getNotify());
        data.put("email", getUser().getEmail());
        data.put("dateTimeAdded", getDateTimeAddedString());
        data.put("color", getColor());
        return data;
    }

    /**
     * @return The id attribute
     */
    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    /**
     * @return The name attribute
     */
    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name != null)
            this.name = name;
    }

    /**
     * @return The dateTime attribute
     */
    public Calendar getDateTime() {
        return dateTime;
    }

    /**
     * @return Then dateTime attribute transformed into a string
     */
    public String getDateTimeString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(this.dateTime.getTime());
    }

    private void setDateTime(String sDateTime) {
        if (sDateTime != null) {
            Calendar dateTime = Calendar.getInstance();
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateTime.setTime(sdf.parse(sDateTime));
            } catch (ParseException e) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    dateTime.setTime(sdf.parse(sDateTime));
                } catch (ParseException e2) {
                    e.printStackTrace();
                }
            }
            this.dateTime = dateTime;
        }
    }

    /**
     * @return The description attribute
     */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Then notify attribute
     */
    public boolean getNotify() {
        return notify;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

    /**
     * @return The user attribute
     */
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if (user != null)
            this.user = user;
    }

    /**
     * @return The dateTimeAdded attribute
     */
    public Calendar getDateTimeAdded() {
        return dateTimeAdded;
    }

    /**
     * @return The dateTimeAdded attribute transformed into string
     */
    public String getDateTimeAddedString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(this.dateTimeAdded.getTime());
    }

    public void setDateTimeAdded(String sDateTimeAdded) {
        if (sDateTimeAdded != null) {
            Calendar dateTimeAdded = Calendar.getInstance();
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                dateTimeAdded.setTime(sdf.parse(sDateTimeAdded));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.dateTimeAdded = dateTimeAdded;
        }
    }

    /**
     * @return The color attribute
     */
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
