package com.example.proyecto_01.model;

import java.util.HashMap;

public class Event extends Deadline {

    private int duration;
    private String location;

    public Event(HashMap<String, Object> data) {
        super(data);
        this.update(data);
    }

    /**
     * Updates the information so that it matches that of the given HashMap
     *
     * @param data HashMap with the new data
     */
    @Override
    public void update(HashMap<String, Object> data) {
        super.update(data);
        setDuration((int) data.get("duration"));
        setLocation((String) data.get("location"));
    }

    /**
     * Formats the required information and dumps it into a HashMap.
     *
     * @return The HashMap with the instance's data
     */
    @Override
    public HashMap<String, Object> getDataForDB() {
        HashMap<String, Object> data = super.getDataForDB();
        data.put("duration", getDuration());
        data.put("location", getLocation());
        return data;
    }

    /**
     * @return The duration attribute
     */
    public int getDuration() {
        return duration;
    }

    private void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * @return The location attribute
     */
    public String getLocation() {
        return location;
    }

    private void setLocation(String location) {
        this.location = location;
    }
}
