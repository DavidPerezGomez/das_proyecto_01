package com.example.proyecto_01.model;

import com.example.proyecto_01.EventReminder;
import com.example.proyecto_01.exceptions.NoSessionException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Singleton class to manage the current user's session and it's data.
 */
public class Session {

    private static Session theSession;

    public static final String SORT_BY_NAME = "sort_by_name";
    public static final String SORT_BY_DATE = "sort_by_date";
    public static final String SORT_BY_DATE_ADDED = "sort_by_date_added";

    private User user;
    private ArrayList<Deadline> deadlines;
    private ArrayList<Event> events;
    private DBManager dbManager;
    private String sortMethod;

    private Session() {
        this.dbManager = new DBManager(EventReminder.getContext(), "event_reminder", null, 1);
        this.sortMethod = SORT_BY_DATE;
    }

    /**
     * Fetch the singleton instance.
     *
     * @return Session The session instance
     * @throws NoSessionException If no session exists (the instance is null)
     */
    public static Session getSession() throws NoSessionException {
        if (theSession != null)

            return theSession;
        else
            throw new NoSessionException();
    }

    /**
     * Open a new session for the use with the specified email.
     *
     * @param email The user's email
     * @return True if the user was found and the session was created correctly
     */
    public static boolean openSession(String email) {
        theSession = new Session();

        return theSession.setUser(email);
    }

    /**
     * Ends the current session.
     */
    public static void endSession() {
        theSession = null;
    }

    /**
     * Finds and sets the user and it's data into the session.
     *
     * @param email The user's email
     * @return True if the user was set correctly, False otherwise
     */
    private boolean setUser(String email) {
        // find the user with the given email
        User user = dbManager.getUserByEmail(email);

        if (user != null) {
            // find the user's deadlines and events
            ArrayList<Deadline> deadlines = dbManager.getDeadlinesByEmail(email);
            ArrayList<Event> events = dbManager.getEventsByEmail(email);
            deadlines.forEach((deadline) -> deadline.setUser(user));
            events.forEach((event) -> event.setUser(user));

            this.user = user;
            this.deadlines = deadlines;
            this.events = events;
            this.sort();

            return true;
        }

        return false;
    }

    /**
     * Sets the sorting method for deadlines and events.
     *
     * @param sortMethod The sorting method to be set
     */
    public void setSortMethod(String sortMethod) {
        this.sortMethod = sortMethod;
        this.sort();
    }

    /**
     * Sorts the deadlines and events according to the current sorting method.
     */
    private void sort() {
        Comparator<Deadline> compDeadline = deadlineComparator(sortMethod);
        Comparator<Event> compEvent = eventComparator(sortMethod);
        deadlines.sort(compDeadline);
        events.sort(compEvent);
    }

    /**
     * Create a Deadline Comparator that works according to the given sorting method.
     *
     * @param sortMethod The sorting method to use
     * @return The Deadline Comparator created
     */
    private Comparator<Deadline> deadlineComparator(String sortMethod) {
        switch (sortMethod) {
            case SORT_BY_NAME:

                return (deadline1, deadline2) -> deadline1.getName().compareTo(deadline2.getName());
            case SORT_BY_DATE:
            default:

                return (deadline1, deadline2) -> deadline1.getDateTime().compareTo(deadline2.getDateTime());
            case SORT_BY_DATE_ADDED:

                return (deadline1, deadline2) -> -deadline1.getDateTimeAdded().compareTo(deadline2.getDateTimeAdded());
        }
    }

    /**
     * Create an Event Comparator that works according to the given sorting method.
     *
     * @param sortMethod The sorting method to use
     * @return The Event Comparator created
     */
    private Comparator<Event> eventComparator(String sortMethod) {
        switch (sortMethod) {
            case SORT_BY_NAME:

                return (event1, event2) -> event1.getName().compareTo(event2.getName());
            case SORT_BY_DATE:
            default:

                return (event1, event2) -> event1.getDateTime().compareTo(event2.getDateTime());
            case SORT_BY_DATE_ADDED:

                return (event1, event2) -> -event1.getDateTimeAdded().compareTo(event2.getDateTimeAdded());
        }
    }

    /**
     * Creates a new Deadline with the data and adds it, ordered, to the database
     * and the local list.
     *
     * @param data HashMap with the data to create the Deadline with
     * @return True if the Deadline was successfully created and added to the database
     */
    public boolean addDeadline(HashMap<String, Object> data) {
        // put in the data specific to the session
        data.put("id", dbManager.getNextDeadlineId());
        data.put("user", user);

        Deadline deadline = new Deadline(data);

        // try adding the deadline to the database
        if (dbManager.insertDeadline(deadline.getDataForDB())) {
            this.insertDeadlineSorted(deadline);

            return true;
        } else {

            return false;
        }
    }

    /**
     * Creates a new Event with the data and adds it, ordered, to the database
     * and the local list.
     *
     * @param data HashMap with the data to create the Event with
     * @return True if the Event was successfully created and added to the database
     */
    public boolean addEvent(HashMap<String, Object> data) {
        // put in the data specific to the session
        data.put("id", dbManager.getNextEventId());
        data.put("user", user);

        Event event = new Event(data);

        // try adding the event to the database
        if (dbManager.insertEvent(event.getDataForDB())) {
            this.insertEventSorted(event);

            return true;
        } else {

            return false;
        }
    }

    /**
     * Removes the Deadline with a specific id and substitutes it with a new one
     * created from the given data.
     * <p>
     * In the database, edits the specific deadline to match the one created with
     * the given data.
     *
     * @param id   Id of the Deadline to be edited/substituted
     * @param data HashMap with the data to create the new Deadline with
     * @return True if all the operations were carried out successfully
     */
    public boolean editDeadline(int id, HashMap<String, Object> data) {
        // put in the data specific to the session
        data.put("id", id);
        data.put("user", user);

        Deadline deadline = new Deadline(data);

        // try updating the deadline in the database
        if (dbManager.updateDeadline(id, deadline.getDataForDB())) {

            // find the deadline that was changed in the local list
            for (int index = 0; index < this.deadlines.size(); index++) {
                if (this.deadlines.get(index).getId() == id) {

                    // remove the deadline and add the new one
                    this.deadlines.remove(index);
                    insertDeadlineSorted(deadline);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Removes the Event with a specific id and substitutes it with a new one
     * created from the given data.
     * <p>
     * In the database, edits the specific event to match the one created with
     * the given data.
     *
     * @param id   Id of the Event to be edited/substituted
     * @param data HashMap with the data to create the new Event with
     * @return True if all the operations were carried out successfully
     */
    public boolean editEvent(int id, HashMap<String, Object> data) {
        // put in the data specific to the session
        data.put("id", id);
        data.put("user", user);

        Event event = new Event(data);

        // try updating the event in the database
        if (dbManager.updateEvent(id, event.getDataForDB())) {

            // find the event that was changed in the local list
            for (int index = 0; index < this.events.size(); index++) {
                if (this.events.get(index).getId() == id) {

                    // remove the event and add the new one
                    this.events.remove(index);
                    insertEventSorted(event);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Deletes the Deadline with the specific id both from the local list and from
     * the database.
     *
     * @param id Id of the Deadline to be deleted
     * @return True if the Deadline was deleted successfully, False otherwise
     */
    public boolean deleteDeadline(int id) {
        if (dbManager.deleteDeadline(id)) {

            // find and remove the deadline that was deleted from the database
            for (int index = 0; index < this.deadlines.size(); index++) {
                if (this.deadlines.get(index).getId() == id) {
                    this.deadlines.remove(index);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Deletes the Event with the specific id both from the local list and from
     * the database.
     *
     * @param id Id of the Event to be deleted
     * @return True if the Event was deleted successfully, False otherwise
     */
    public boolean deleteEvent(int id) {
        if (dbManager.deleteEvent(id)) {

            // find and remove the event that was deleted from the database
            for (int index = 0; index < this.events.size(); index++) {
                if (this.events.get(index).getId() == id) {
                    this.events.remove(index);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Inserts a Deadline into the list following the current sorting method.
     *
     * @param deadline Deadline to be inserted
     */
    private void insertDeadlineSorted(Deadline deadline) {
        Comparator<Deadline> comparator = deadlineComparator(this.sortMethod);
        for (int i = 0; i < this.deadlines.size(); i++) {
            Deadline item = this.deadlines.get(i);
            if (comparator.compare(deadline, item) >= 0) {
                this.deadlines.add(i, deadline);

                return;
            }
        }
        this.deadlines.add(deadline);
    }

    /**
     * Inserts an Event into the list following the current sorting method.
     *
     * @param event Event to be inserted
     */
    private void insertEventSorted(Event event) {
        Comparator<Event> comparator = eventComparator(this.sortMethod);
        for (int i = 0; i < this.events.size(); i++) {
            Event item = this.events.get(i);
            if (comparator.compare(event, item) >= 0) {
                this.events.add(i, event);

                return;
            }
        }
        this.events.add(event);
    }

    /**
     * Inverts the value "notify" for the Deadline with the specified id both in the
     * local list and in the database.
     *
     * @param id Id of the Deadline
     * @return New value of the "notify" property or False if the Deadline was not found
     */
    public boolean toggleDeadlineNotify(int id) {
        Deadline deadline = findDeadlineById(id);
        if (deadline != null) {
            boolean newNotify = !deadline.getNotify();

            // try updating the value on the database
            if (dbManager.setDeadlineNotify(id, newNotify)) {
                deadline.setNotify(newNotify);

                return newNotify;
            }

            return !newNotify;
        }

        return false;
    }

    /**
     * Inverts the value "notify" for the Event with the specified id both in the
     * local list and in the database.
     *
     * @param id Id of the Event
     * @return New value of the "notify" property or False if the Event was not found
     */
    public boolean toggleEventNotify(int id) {
        Event event = findEventById(id);
        if (event != null) {
            boolean newNotify = !event.getNotify();
            if (dbManager.setEventNotify(id, newNotify)) {
                event.setNotify(newNotify);

                return newNotify;
            }

            return !newNotify;
        }

        return false;
    }

    /**
     * Finds a Deadline with a specific id.
     *
     * @param id The id of the Deadline to be found
     * @return The Deadline with matching id. Null if no matches were found
     */
    public Deadline findDeadlineById(int id) {
        for (int i = 0; i < this.deadlines.size(); i++) {
            Deadline deadline = this.deadlines.get(i);
            if (deadline.getId() == id) {

                return deadline;
            }
        }

        return null;
    }

    /**
     * Finds an Event with a specific id.
     *
     * @param id The id of the Event to be found
     * @return The Event with matching id. Null if no matches were found
     */
    public Event findEventById(int id) {
        for (int i = 0; i < this.events.size(); i++) {
            Event event = this.events.get(i);
            if (event.getId() == id) {

                return event;
            }
        }

        return null;
    }

    /**
     * @return The current user's email
     */
    public String getEmail() {

        return user.getEmail();
    }

    /**
     * @return The current user's name
     */
    public String getName() {

        return user.getName();
    }

    /**
     * @return The current user's deadline list
     */
    public ArrayList<Deadline> getUserDeadlines() {

        return deadlines;
    }

    /**
     * @return The current user's event list
     */
    public ArrayList<Event> getUserEvents() {

        return events;
    }

}
