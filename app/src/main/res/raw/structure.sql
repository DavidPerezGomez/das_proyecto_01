CREATE TABLE user (
	email VARCHAR(40),
	name VARCHAR(40),
	password_hash VARCHAR(200),
	PRIMARY KEY(email)
);

CREATE TABLE deadline (
	user_email VARCHAR(40) NOT NULL,
	name VARCHAR(40) NOT NULL,
	date_time DATETIME NOT NULL,
	description TEXT NOT NULL,
	notify BOOLEAN NOT NULL,
	date_added DATETIME NOT NULL,
	color VARCHAR(7) NOT NULL,
	PRIMARY KEY(user_email, name, date_time),
	FOREIGN KEY(user_email)
	REFERENCES user(email)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

CREATE TABLE event (
	user_email VARCHAR(40) NOT NULL,
	name VARCHAR(40) NOT NULL,
	date_time DATETIME NOT NULL,
	description TEXT NOT NULL,
	notify BOOLEAN NOT NULL,
	duration INT NOT NULL,
	location VARCHAR(100) NOT NULL,
	date_added DATETIME NOT NULL,
	color VARCHAR(7) NOT NULL,
	PRIMARY KEY(user_email, name, date_time),
	FOREIGN KEY(user_email)
	REFERENCES user(email)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);
